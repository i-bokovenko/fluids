
M = {}

local function contains(list, item)
	for i = 1, #list do
		if list[i] == item then
			return true
		end
	end
	return false
end

function M.doUnowDaWay(array, stack, to)

	local last = stack[#stack]

	if last == to then
		return true
	end

	for curr = 1, #array[last] do

		if array[last][curr] == 0 and not contains(stack, curr) then

			table.insert(stack, curr)

			if M.doUnowDaWay(array, stack, to) then
				return true
			end

			table.remove(stack, #stack)
		end
	end

	return false
end

function M.to_string(list)
	local str = ""
	for i = 1, #list do
		if i == 1 then
			str = str .. tostring(list[i])
		else
			str = str .. " -> " .. tostring(list[i])
		end
	end
	return str
end

function M.to_string_graph(graph)
	local str = ""
	for i = 1, #graph do
		for j = 1, #graph do
			str = str .. graph[i][j] .. " "
		end
		str = str .. "\n"
	end
	return str
end

return M
-- local graph = {{0,1,0}, {0,0,1}, {0,1,0}}
-- local stk = {1}
-- 
-- 
-- for i = 1, #graph do
-- 	for j = 1, #graph do
-- 
-- 		stk = {i}
-- 
-- 		local result = doUnowDaWay(graph,stk,j) and "way: " or "noway\n"
-- 
-- 		io.write(i.." -> "..j .. " " .. result)
-- 
-- 		if result == "way: " then
-- 			printStack(stk)
-- 		end
-- 	end
-- end


